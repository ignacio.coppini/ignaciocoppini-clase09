// Obtener los elementos del formulario
const formulario = document.getElementById('formulario');
const nombreCompleto = document.getElementById('nombreCompleto');
const email = document.getElementById('email');
const password = document.getElementById('password');
const confirmPassword = document.getElementById('confirmPassword');
const edad = document.getElementById('edad');
const telefono = document.getElementById('telefono');
const direccion = document.getElementById('direccion');
const ciudad = document.getElementById('ciudad');
const codigoPostal = document.getElementById('codigoPostal');
const dni = document.getElementById('dni');

// Validar nombre completo
nombreCompleto.addEventListener('blur', () => {
  if (nombreCompleto.value.trim().length < 6 || !nombreCompleto.value.includes(' ')) {
    nombreCompleto.classList.add('is-invalid');
    nombreCompleto.nextElementSibling.textContent = 'Ingrese un nombre completo válido';
  } else {
    nombreCompleto.classList.remove('is-invalid');
    nombreCompleto.nextElementSibling.textContent = '';
  }
});

// Validar email
email.addEventListener('blur', () => {
  const regexEmail = /^\S+@\S+\.\S+$/;
  if (!regexEmail.test(email.value)) {
    email.classList.add('is-invalid');
    email.nextElementSibling.textContent = 'Ingrese un email válido';
  } else {
    email.classList.remove('is-invalid');
    email.nextElementSibling.textContent = '';
  }
});

// Validar contraseña
password.addEventListener('blur', () => {
  const regexPassword = /^(?=.*[a-zA-Z])(?=.*\d).{8,}$/;
  if (!regexPassword.test(password.value)) {
    password.classList.add('is-invalid');
    password.nextElementSibling.textContent = 'La contraseña debe tener al menos 8 caracteres y contener letras y números';
  } else {
    password.classList.remove('is-invalid');
    password.nextElementSibling.textContent = '';
  }
});

// Validar repetir contraseña
confirmPassword.addEventListener('blur', () => {
  if (confirmPassword.value !== password.value) {
    confirmPassword.classList.add('is-invalid');
    confirmPassword.nextElementSibling.textContent = 'Las contraseñas no coinciden';
  } else {
    confirmPassword.classList.remove('is-invalid');
    confirmPassword.nextElementSibling.textContent = '';
  }
});

// Validar edad
edad.addEventListener('blur', () => {
  if (parseInt(edad.value) < 18) {
    edad.classList.add('is-invalid');
    edad.nextElementSibling.textContent = 'Debe ser mayor o igual a 18 años';
  } else {
    edad.classList.remove('is-invalid');
    edad.nextElementSibling.textContent = '';
  }
});

// Validar teléfono
telefono.addEventListener('blur', () => {
  const regexTelefono = /^\d{7,}$/;
  if (!regexTelefono.test(telefono.value)) {
    telefono.classList.add('is-invalid');
    telefono.nextElementSibling.textContent = 'Ingrese un número de teléfono válido';
  } else {
    telefono.classList.remove('is-invalid');
    telefono.nextElementSibling.textContent = '';
  }
});

// Validar dirección
direccion.addEventListener('blur', () => {
  if (direccion.value.trim().length < 5) {
    direccion.classList.add('is-invalid');
    direccion.nextElementSibling.textContent = 'Ingrese una dirección válida';
  } else {
    direccion.classList.remove('is-invalid');
    direccion.nextElementSibling.textContent = '';
  }
});

// Validar ciudad
ciudad.addEventListener('blur', () => {
  if (ciudad.value.trim().length < 3) {
    ciudad.classList.add('is-invalid');
    ciudad.nextElementSibling.textContent = 'Ingrese una ciudad válida';
  } else {
    ciudad.classList.remove('is-invalid');
    ciudad.nextElementSibling.textContent = '';
  }
});

// Validar código postal
codigoPostal